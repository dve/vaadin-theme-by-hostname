package net.vergien;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;

import org.jsoup.nodes.Element;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.BootstrapFragmentResponse;
import com.vaadin.server.BootstrapListener;
import com.vaadin.server.BootstrapPageResponse;
import com.vaadin.server.ServiceException;
import com.vaadin.server.SessionInitEvent;
import com.vaadin.server.SessionInitListener;
import com.vaadin.server.VaadinService;
import com.vaadin.server.VaadinServlet;

@WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
public class MyServlet extends VaadinServlet {
	private MyUIProvider uiProvider = new MyUIProvider();

	@Override
	protected void servletInitialized() throws ServletException {
		super.servletInitialized();

		getService().addSessionInitListener(new SessionInitListener() {
			@Override
			public void sessionInit(SessionInitEvent event) throws ServiceException {
				event.getSession().addUIProvider(uiProvider);
			}
		});
		
		VaadinService.getCurrent().addSessionInitListener((com.vaadin.server.SessionInitEvent e) -> {
			e.getSession().addBootstrapListener(new BootstrapListener() {
				@Override
				public void modifyBootstrapFragment(BootstrapFragmentResponse response) {
				}

				@Override
				public void modifyBootstrapPage(BootstrapPageResponse response) {
					Element head = response.getDocument().getElementsByTag("head").get(0);
					Element meta = head.appendElement("meta");
					meta.attr("name", "viewport");
					meta.attr("content", "width=device-width, initial-scale=1");
				}
			});
		});
	}
}
