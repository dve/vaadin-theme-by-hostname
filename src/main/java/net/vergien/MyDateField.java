package net.vergien;

import org.vaadin.viritin.util.HtmlElementPropertySetter;

import com.vaadin.ui.DateField;

public class MyDateField extends DateField {
	private HtmlElementPropertySetter heps;

	public MyDateField(String caption) {
		super(caption);
	}

	@Override
	public void beforeClientResponse(boolean initial) {
		super.beforeClientResponse(initial);
		if (heps == null) {
			
			heps = new HtmlElementPropertySetter(this);
		}
		
		
		heps.setProperty("child::input[1]","type", "date");
	}
}
