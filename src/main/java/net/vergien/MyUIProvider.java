package net.vergien;

import javax.servlet.ServletRequest;

import com.vaadin.server.UIClassSelectionEvent;
import com.vaadin.server.UICreateEvent;
import com.vaadin.server.UIProvider;
import com.vaadin.server.VaadinServletRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

public class MyUIProvider extends UIProvider {
    @Override
    public String getTheme(UICreateEvent event) {
        ServletRequest reqeust = ((VaadinServletRequest) event.getRequest()).getHttpServletRequest();

        String hostname = reqeust.getServerName();
        String theme = null;
        switch (hostname) {
        case "localhost":
        case "wiesel":
            theme = "valo";
            break;
        default:
            theme = "mytheme";
            break;
        }

        return theme;
    }

    private void setMobileParameter(UIClassSelectionEvent event) {
        boolean mobileUserAgent = event.getRequest().getHeader("user-agent").toLowerCase().contains("mobile");
        boolean mobileParameter = event.getRequest().getParameter("mobile") != null;

        Boolean forceMobile = (Boolean) VaadinSession.getCurrent().getAttribute("forceMobile");
        if (forceMobile == null) {
            VaadinSession.getCurrent().setAttribute("mobile", mobileUserAgent);
        } else {
            VaadinSession.getCurrent().setAttribute("mobile", forceMobile);
        }
    }

    @Override
    public Class<? extends UI> getUIClass(UIClassSelectionEvent event) {
        setMobileParameter(event);
        return MyUI.class;
    }
}
