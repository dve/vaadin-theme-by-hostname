package net.vergien;

import org.vaadin.viritin.fields.EmailField;

import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.DateField;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        boolean mobile = (boolean) VaadinSession.getCurrent().getAttribute("mobile");

        final VerticalLayout layout = new VerticalLayout();

        final TextField name = new TextField();
        name.setCaption("Type your name here:");

        EmailField email = new EmailField("Email");
        DateField dateOfBirth = new MyDateField("Date of Birth");
        dateOfBirth.setDateFormat("dd.MM.yyyy");
        System.out.println(VaadinSession.getCurrent().getLocale());

        System.out.println(dateOfBirth.getDateFormat());

        Button button = new Button("Click Me");
        button.addClickListener(e -> {
            layout.addComponent(new Label("Thanks " + name.getValue() + ", it works!" + dateOfBirth.getValue()));
        });
        layout.addComponents(name, email, dateOfBirth, button);
        layout.setMargin(true);
        layout.setSpacing(true);

        if (mobile) {
            layout.addComponent(new Label("mobile"));
            name.setWidth("100%");
            button.setWidth("100%");
        } else {

        }
        setContent(layout);
    }

}
